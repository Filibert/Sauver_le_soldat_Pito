package Creatures

import Helper.{Attack, ConditionHelper, Dice}
import World.World
import org.apache.spark.graphx
import org.apache.spark.graphx.{EdgeTriplet, VertexId}

import scala.collection.mutable.ArrayBuffer

abstract class Creature(val team_param: Int) extends Serializable
{

  var name = ""
  var armor = 0
  var damageReduction = 0
  var hp = 0
  var Messages = new ArrayBuffer[(VertexId,String)]
  var needHelp = false
  var maxHp = 0

  var canMelee = false
  var distMelee = 0
  var attackMeleeNb = 0
  var attackMeleePrecision: ArrayBuffer[Int] = ArrayBuffer[Int]()
  var attackMelee = new Attack(0,0,0,attackMeleePrecision)

  var canRange = false
  var distRange = 0
  var attackRangeNb = 0
  var attackRangePrecision: ArrayBuffer[Int] = ArrayBuffer[Int]()
  var attackRange = new Attack(0,0,0,attackRangePrecision)

  var healCounter = 0
  var casterLevel = 0


  var isAlive = true
  var regen = 0
  var moveDist = 0
  var xPos = 0
  var yPos = 0
  var zPos = 0
  var init = 0
  var team: Int = team_param

  var isTargetable = true

  def Play (id: Long): Unit = {
    hp += regen
    World.curentAliveMembers.filter(_._1 == id)(0)._2.hp += regen
    var max_range = distMelee
    if(distRange > 0) max_range = distRange
    var creature = findWeakestCreature(World.curentAliveMembers, max_range)
    if(isAlive) {
      var distance = getDistance(creature._2)
      if(distance > max_range)
        move(World.gameArea, id)
      attack()
    }
  }

  def attack() : Unit ={

    var max_range = distMelee
    // if aliveMembers is Empty, the fight should be stopped before
    assert(World.curentAliveMembers.nonEmpty)
    if(distRange > 0) max_range = distRange
    var creature = findWeakestCreature(World.curentAliveMembers, max_range)
    attack(creature)

  }

  def attack(target: (Long,Creature)) : Unit = {
    var max_range = distMelee
    var range = distMelee
    var attack = attackMelee
    var nbAttack = attackMeleeNb

    // if aliveMembers is Empty, the fight should be stopped before
    assert(World.curentAliveMembers.nonEmpty)
    if(distRange > 0) max_range = distRange
    var creature = target
    if(!classOf[Orc].isInstance(this))println(name + " target : " + creature._2.name)
    var distance = getDistance(creature._2)
    if(distance > max_range) {
      if(!classOf[Orc].isInstance(this)) println("No ennemies are close enough to be attacked")
      return
    }

    if(distance > distMelee){
      range = distRange
      attack = attackRange
      nbAttack = attackRangeNb
    }

    for (i <- 0 until nbAttack) {
      val attackStats = attack.calculateDamagesAndPrecision(i)
      if (creature._2.armor <= attackStats._2 && isAlive) {
        var finalDamage = attackStats._1 - creature._2.damageReduction
        if (finalDamage > 0) {
          creature._2.hp -= finalDamage
          if(!classOf[Orc].isInstance(this)) print(name + " attacks " + creature._2.name + " who lost " + finalDamage + " hp")
          if (creature._2.hp <= 0) {
            if(!classOf[Orc].isInstance(this))  print(" and kills him")
            creature._2.isAlive = false

            if( World.queueCreatureWhoHaventPlay.exists(_._1 == creature._1)){
              World.queueCreatureWhoHaventPlay.filter(_._1 == creature._1).head._2.isAlive = false
            }
            World.KillACreature(creature._1)
            if(ConditionHelper.GameIsNotOver()){
              creature = findWeakestCreature(World.curentAliveMembers, range)
              distance = getDistance(creature._2)
              if(distance > range) {
                if(!classOf[Orc].isInstance(this))  print("No more ennemies in range")
              }
            }
            else {
              println()
              return
            }
          }
        }
        else {
          if(!classOf[Orc].isInstance(this)) print(name + " attacks " + creature._2.name + "who lost no hp (attack: " + attackStats._1 + " / damage reduction: " + creature._2.damageReduction + ")" )
        }
        if(!classOf[Orc].isInstance(this))  println()
      }
      else if(!classOf[Orc].isInstance(this)) println("Oh no " + name + " miss is shot")
    }
  }

  def heal(): Unit ={
    var healCounter = this.healCounter
    var aliesWhoNeedHelpNb = Messages.size
    for (i <- 0 until  aliesWhoNeedHelpNb){
      var target = World.curentAliveMembers.filter(_._1 == Messages(0)._1)
      Messages -= Messages(0)
      if(target.nonEmpty){
        if(target(0)._2.isAlive && target(0)._2.needHelp){
          while(target(0)._2.needHelp){
            val power = 10 * Math.min(casterLevel,15)
            target(0)._2.hp = Math.min(target(0)._2.hp + power, target(0)._2.maxHp)
            println(target(0)._2.name + " have been healed by " + power)
            healCounter -= 1
            if (target(0)._2.hp/target(0)._2.maxHp > 0.4 && Messages.nonEmpty)
              target(0)._2.needHelp = false
            if(healCounter == 0)
              return
          }
        }
      }
    }
  }

  def findWeakestCreature(aliveMembers: ArrayBuffer[(Long,Creature)], limit: Int): (Long,Creature) = {
    val teamArray = createTeamArray(aliveMembers, (this.team + 1)%2)
    var weakestCreature = teamArray(0)
    var distance: Double = 0
    teamArray.foreach(member => {
      distance = getDistance(member._2)
      if (member._2.hp < weakestCreature._2.hp && distance < limit && isTargetable) weakestCreature = member
    })
    weakestCreature
  }

  def findWeakestCreature(name : String, limit: Int): (Long,Creature) = {
   if(World.curentAliveMembers.filter(_._2.name == name).nonEmpty)
    return findWeakestCreature(World.curentAliveMembers.filter(_._2.name == name), limit)
   else
     return null
  }


  def getDistance(target: Creature): Double ={
    Math.sqrt(sqr(this.xPos - target.xPos) + sqr(this.yPos - target.yPos) + sqr(this.zPos - target.zPos))
  }
  def sqr(x: Int): Int = x * x
  def move(limit: Int,id :Long): Unit ={
      var x = Dice.ThrowDice(1, moveDist)
      var y = Dice.ThrowDice(1, moveDist)
      if (xPos > 0) x *= -1
      if (yPos > 0) y *= -1
      xPos += x
      yPos += y
      World.MoveACreature(id,x,y)
  }

  private def createTeamArray(aliveMembers: ArrayBuffer[(Long, Creature)], team: Int): ArrayBuffer[(Long,Creature)] = {
    val wArray = aliveMembers.filter(creature => {creature._2.team == team})
    // Both teams should be represented here, else the fight is already over !
    assert(wArray.nonEmpty)
    wArray
  }

  override def toString = s"Creature($name, $hp, ($xPos,$yPos))"
}

