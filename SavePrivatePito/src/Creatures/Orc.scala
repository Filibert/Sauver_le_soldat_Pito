package Creatures

import Helper.{Attack, Dice}

class Orc (orcType: String, area: Int) extends Creature(1){
  name = orcType
  name match{
    case "WarLord" =>
      canMelee = true
      distMelee = 10
      attackMeleeNb = 3
      attackMeleePrecision += 19 += 14 += 9
      attackMelee = new Attack(10,1,8,attackMeleePrecision)

      canRange = true
      distRange = 50
      attackRangeNb = 1
      attackRangePrecision += 19
      attackRange = new Attack(5,1,6,attackRangePrecision)

      armor = 27
      hp = 265 + Dice.ThrowDice(13, 10)
      moveDist = 30
      xPos = Dice.ThrowDice(1, 2 * area) - area
      yPos = Dice.ThrowDice(1, 2 * area) - area
      init = 2

    case "Worgs Rider" =>
      canMelee = true
      distMelee = 10
      attackMeleeNb = 3   //x3 on the website
      attackMeleePrecision += 6 += 6 += 6
      attackMelee = new Attack(2,1,8,attackMeleePrecision)

      canRange = true
      distRange = 40
      attackRangeNb = 3   //x3 on the website
      attackRangePrecision += 4 += 4 += 4
      attackRange = new Attack(0,1,6,attackRangePrecision)

      armor = 18
      hp = 2 + Dice.ThrowDice(2, 10)
      moveDist = 20
      xPos = Dice.ThrowDice(1, 2 * area) - area
      yPos = Dice.ThrowDice(1, 2 * area) - area
      init = 2

    case "Half-Orc Barbarian" =>
      canMelee = true
      distMelee = 10
      attackMeleeNb = 3
      attackMeleePrecision += 20 += 15 += 10
      attackMelee = new Attack(10,1,8,attackMeleePrecision)

      canRange = true
      distRange = 70
      attackRangeNb = 3
      attackRangePrecision += 16 += 11 += 6
      attackRange = new Attack(6,1,8,attackRangePrecision)

      armor = 17
      hp = 65 + Dice.ThrowDice(11, 12)
      moveDist = 40
      xPos = Dice.ThrowDice(1, 2 * area) - area
      yPos = Dice.ThrowDice(1, 2 * area) - area
      init = 4

  }
}

