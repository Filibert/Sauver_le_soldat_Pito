package Creatures
import Helper.{Attack, Dice}

class Angel extends Creature(0){


  override def Play(id : Long): Unit  = {
    hp += regen
    World.World.curentAliveMembers.filter(_._1 == id)(0)._2.hp += regen
    if(hp.toFloat/maxHp.toFloat <= 0.9){
      needHelp = true
      World.World.SetNeedHelp(id)
      println(name + " say ALED " + id)
    }
    var max_range = distMelee
    if(distRange > 0) max_range = distRange
    var dragon = World.World.curentAliveMembers.filter(_._2.name == "GreenGreat Wyrm Dragon")
    var isDragonAliveAndCloseEnough = dragon.nonEmpty
    if(isDragonAliveAndCloseEnough) isDragonAliveAndCloseEnough = getDistance(dragon(0)._2) <= max_range
    var  angelSlayer = findWeakestCreature("Angel Slayer",max_range)
    var anAngelSlayerIsCloseEnough =  if(angelSlayer == null)false else getDistance(angelSlayer._2) <= max_range

    if(Messages.isEmpty || healCounter == 0 ){
      if(!isDragonAliveAndCloseEnough && !anAngelSlayerIsCloseEnough){
        attack()
      }else if( isDragonAliveAndCloseEnough && !anAngelSlayerIsCloseEnough){
        attack(dragon(0))
      }else{
        attack(angelSlayer)
      }

    }
    else{
      heal()
    }

  }
}

class Solar (area: Int) extends Angel {
  name = "Solar"
  armor = 44
  damageReduction = 15
  hp = 242 + Dice.ThrowDice(22, 10)
  maxHp = hp

  canMelee = true
  distMelee = 10
  attackMeleeNb = 4
  attackMeleePrecision += 35 += 30 += 25 += 20
  attackMelee = new Attack(18,3,6,attackMeleePrecision)

  canRange = true
  distRange = 110
  attackRangeNb = 4   //x3 on the website
  attackRangePrecision += 31 += 26 += 21 += 16
  attackRange = new Attack(14,2,6,attackRangePrecision)

  healCounter = 3
  casterLevel = 20

  regen = 15
  moveDist = 50
  xPos = Dice.ThrowDice(1, 2 * area) - area
  yPos = Dice.ThrowDice(1, 2 * area) - area
  init = 9

}


class Planetar (area: Int) extends Angel {
  name = "Planetar"
  armor = 32
  hp = 136 + Dice.ThrowDice(17,10) // 229
  maxHp = hp


  canMelee = true
  distMelee = 10
  attackMeleeNb = 3
  attackMeleePrecision += 27 += 22 += 17
  attackMelee = new Attack(15,3,6,attackMeleePrecision)

  canRange = false

  casterLevel

  regen = 10
  moveDist = 30
  xPos = Dice.ThrowDice(1, 2 * area) - area
  yPos = Dice.ThrowDice(1, 2 * area) - area
  init = 8
}


class Movanic_Deva (area: Int) extends Angel {
  name = "Movanic_Deva"
  armor = 32
  hp = 60 + Dice.ThrowDice(12,10) // 126
  maxHp = hp


  canMelee = true
  distMelee = 10
  attackMeleeNb = 3
  attackMeleePrecision += 17 += 12 += 7
  attackMelee = new Attack(7,2,6,attackMeleePrecision)

  canRange = false

  casterLevel =  9


  regen =  0
  moveDist = 40
  xPos = Dice.ThrowDice(1, 2 * area) - area
  yPos = Dice.ThrowDice(1, 2 * area) - area
  init = 7
}

class Astral_Deva (area: Int) extends Angel {
  name = "Astral_Deva"
  armor = 29
  hp = 90 + Dice.ThrowDice(15,10) // 173
  maxHp = hp


  canMelee = true
  distMelee = 10
  attackMeleeNb = 3
  attackMeleePrecision += 26 += 21 += 16
  attackMelee = new Attack(14,1,8,attackMeleePrecision)

  canRange = false

  casterLevel =  13

  healCounter = 1


  regen =  0
  moveDist = 50
  xPos = Dice.ThrowDice(1, 2 * area) - area
  yPos = Dice.ThrowDice(1, 2 * area) - area
  init = 7
}