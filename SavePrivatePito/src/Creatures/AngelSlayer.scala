package Creatures

import Helper.{Attack, Dice}

class AngelSlayer(area : Int) extends Creature(1){
  name = "Angel Slayer"
  armor = 26
  hp = 25 + Dice.ThrowDice(15,10) // 112
  maxHp = hp

  canMelee = true
  distMelee = 10
  attackMeleeNb = 3
  attackMeleePrecision += 21 += 16 += 11
  attackMelee = new Attack(7,1,8,attackMeleePrecision)

  canRange = true
  distRange = 110
  attackRangeNb = 3
  attackRangePrecision += 19 += 14 += 9
  attackRange = new Attack(6,1,8,attackRangePrecision)

  healCounter = 3
  casterLevel = 12

  moveDist = 40
  xPos = Dice.ThrowDice(1, 2 * area) - area
  yPos = Dice.ThrowDice(1, 2 * area) - area
  init = 7

  override def Play(id: Long): Unit = {
    if(Messages.size == 0 || healCounter == 0){
      super.Play(id)
    }
    else{
      heal()
    }

  }
}
