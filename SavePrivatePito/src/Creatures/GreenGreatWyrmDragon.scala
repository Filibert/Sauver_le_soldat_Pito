package Creatures

import Helper.{Attack, Dice}
import World.World

class GreenGreatWyrmDragon(area : Int) extends Creature(1) {
  name = "Citizen"
  armor = 37
  hp = 216 + Dice.ThrowDice(27,12) //391
  maxHp = hp

  canMelee = true
  distMelee = 25
  attackMeleeNb = 1
  attackMeleePrecision += 33
  attackMelee = new Attack(21,4,8,attackMeleePrecision)

  canRange = false

  healCounter = 0
  casterLevel = 27

  regen = 15
  moveDist = 40
  xPos = Dice.ThrowDice(1, 2 * area) - area
  yPos = Dice.ThrowDice(1, 2 * area) - area
  init = 2

  isTargetable = false

  def ShowHimSelf(): Unit = {

    name = "GreenGreat Wyrm Dragon"
    isTargetable = true

    var d = World.curentAliveMembers.filter(_._2.name == "Citizen")
    if(d.nonEmpty){
      d(0)._2.isTargetable = true
      d(0)._2.name = "GreenGreat Wyrm Dragon"
    }

  }

  override def Play(id: Long): Unit = {

    if(hp.toFloat/maxHp.toFloat <= 0.9){
      needHelp = true
      World.SetNeedHelp(id)
      println(name + " say ALED " + id)
    }

    var max_range = distMelee
    if(distRange > 0) max_range = distRange
    var creature = findWeakestCreature(World.curentAliveMembers, max_range)
    var distance = getDistance(creature._2)
    if(isAlive) {
      println(isTargetable)
      if(isTargetable)
      {
        if (distance > max_range)
          move(World.gameArea, id)
        attack()
      }
      else if(World.turn < 3){
        var Solar = World.curentAliveMembers.filter(_._2.name == "Solar")
        if(Solar.nonEmpty){
          var distancetoSolar = getDistance(Solar(0)._2)
          move(World.gameArea, id)
          if(distancetoSolar <= max_range){
            println("Le dragon vert se dévoile s'envole et attaque le Solar")
            ShowHimSelf()
            Fly()
            attack(Solar(0))
          }
        }
      }
      else{
        println("Le dragon vert se dévoile s'envole ")
        ShowHimSelf()
        Fly()
      }
    }
  }
  def Fly(): Unit ={
    zPos = 10
    World.FindACreature("GreenGreat Wyrm Dragon")._2.zPos = 10
  }

}