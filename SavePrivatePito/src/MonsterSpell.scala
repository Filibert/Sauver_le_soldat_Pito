import org.apache.spark.sql.{Row, SparkSession}
import scala.collection.mutable

object  MonsterSpell {

  def g(v:Row) = v.getAs[mutable.WrappedArray[String]]("spells").mkString(" ").split("\\s+").filterNot(e => e.isEmpty()).map(spell => (spell, v.getAs[String]("name")))

  def main(args: Array[String])
  {
    val spark_session  = SparkSession
      .builder()
      .appName("SavePrivatePito")
      .master("local[*]")
      .getOrCreate()

    import spark_session.implicits._

    // A JSON dataset is pointed to by path.
    // The path can be either a single text file or a directory storing text files
    val path = "../Scrapper/result.json"
    val monsterDF = spark_session.read.json(path)

    val mapRDD: org.apache.spark.rdd.RDD[(String, String)] = monsterDF.flatMap(x => g(x)).rdd
    val reducedrdd = mapRDD.reduceByKey(_+ " " + _)
    val result = reducedrdd.map(t => (t._1, t._2.split("\\s+"))).toDF("Spell", "Monsters")

    result.printSchema()
    result.show()
    // Print all results
//    result.collect().foreach(println)

    val resultRDD: org.apache.spark.rdd.RDD[Row] = result.rdd

    // Write results to file
//    resultRDD.saveAsTextFile("result")







  }
}
