package World

import Creatures._
import Helper.ConditionHelper
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object World {
  var winner = 2
  var isGameOver = false
  var curentAliveMembers: ArrayBuffer[(VertexId, Creature)] = ArrayBuffer[(Long,Creature)]()
  var queueCreatureWhoHaventPlay: mutable.Queue[(VertexId, Creature)] = mutable.Queue[(Long,Creature)]()
  var aliveMembers : RDD[(Long,Creature)] = _
  val edges: ArrayBuffer[Edge[Boolean]] = ArrayBuffer[Edge[Boolean]]()
  var gameArea : Int = _
  var angelArea : Int = _
  var superGraph : Graph[Creature, Boolean] = _
  var messages: ArrayBuffer[(VertexId, ArrayBuffer[(VertexId, String)])] = ArrayBuffer[(Long,ArrayBuffer[(VertexId, String)])]()
  var context :SparkContext = _
  var turn = 0
  def InitFirstFight( sc :SparkContext): Unit =
  {
    context = sc
    gameArea = 100
    angelArea = 10
    var i = 0
    var allMember = ArrayBuffer[Creature]()
    allMember += new Solar(angelArea)
    allMember += new Orc("WarLord", gameArea)
    for(i <- 0 until 9)
      allMember += new Orc("Worgs Rider", gameArea)

    for(i <- 0 until 4)
      allMember += new Orc("Half-Orc Barbarian", gameArea)


    InitSuperGraph(allMember)
  }
  def InitSecondFight(sc :SparkContext) : Unit =
  {
    context = sc
    gameArea = 100
    angelArea = 10
    var i = 0
    var allMember = ArrayBuffer[Creature]()

    allMember += new Solar(angelArea)
    for(i <- 0 until 2) allMember += new Planetar(gameArea)
    for(i <- 0 until 2) allMember += new Movanic_Deva(gameArea)
    for(i <- 0 until 5) allMember += new Astral_Deva(gameArea)

    allMember += new GreenGreatWyrmDragon(gameArea)
    for(i <- 0 until 10) allMember += new AngelSlayer(gameArea)
    for(i <- 0 until 200) allMember += new Orc("Half-Orc Barbarian", gameArea)

    InitSuperGraph(allMember)
  }
  def InitSuperGraph(creatures: ArrayBuffer[Creature] ) : Unit =
  {
    var j  = 0
    creatures.foreach(f => {
      j += 1
      for( i <- j until creatures.size){
        edges += new Edge[Boolean](j-1,i, creatures(i).team != f.team)
        edges += new Edge[Boolean](i,j-1, creatures(i).team != f.team)
      }
    })

    aliveMembers = context.parallelize(creatures).zipWithIndex().map(f => (f._2,f._1))
    curentAliveMembers = aliveMembers.collect().to[ArrayBuffer]
    val RDDedges = context.parallelize(edges)
    superGraph = Graph(aliveMembers,RDDedges)
    superGraph.unpersist()
  }

  private def UpdateMessages(): Unit = {
     var messages = superGraph.aggregateMessages[(ArrayBuffer[(VertexId,String)])](triplet =>{
      if(triplet.srcAttr.needHelp && !triplet.attr && triplet.dstAttr.casterLevel != 0){
        var arrayBuffer = new ArrayBuffer[(VertexId,String)]()
        arrayBuffer += Tuple2(triplet.srcId, "help")
        triplet.sendToDst(arrayBuffer)
      }
    },(a,b) =>
    {a++b},TripletFields.All)
    if(this.messages.nonEmpty) this.messages.remove(0,this.messages.size-1)
    messages.foreach(f=>{
      if(f._2.nonEmpty){
        this.messages += f
      }
    })
  }

  def UpdateGraph(i : Int): Unit = {
    superGraph = superGraph.joinVertices(superGraph.vertices.map[(VertexId,Creature)](f => {
      var monster  = f._2
      monster.isAlive = false
      (f._1,monster)
    }))((id,creature,newCreature) => newCreature).joinVertices(aliveMembers)((id,creature,newCreature) => {
      newCreature
    }).subgraph(vpred = (id, attr) => attr.isAlive)
    if (i%20 == 0)
      superGraph.checkpoint()
  }

  private def UpdateCurrentMembers(): Unit = {
    queueCreatureWhoHaventPlay = superGraph.vertices.collect().to[mutable.Queue]
    while (queueCreatureWhoHaventPlay.nonEmpty){
      var creature = queueCreatureWhoHaventPlay.dequeue
      var messages =  this.messages.filter(_._1 == creature._1)

      if(creature._2.isAlive && !isGameOver){

        if(messages.nonEmpty)
          creature._2.Messages = messages(0)._2

        if(!classOf[Orc].isInstance(creature._2)) println(creature._2.name +" is playing")
        creature._2.Play(creature._1)
        if(!classOf[Orc].isInstance(creature._2)) println()
      }
    }
    UpdateAliveMembers
  }

  def UpdateAliveMembers(): Unit = {
    aliveMembers = context.parallelize(curentAliveMembers)
  }

  def KillACreature(vertexId: VertexId): Unit = {
    curentAliveMembers.filter(_._1 == vertexId)(0)._2.isAlive = false
    curentAliveMembers -= curentAliveMembers.filter(_._1 == vertexId)(0)
    isGameOver = !ConditionHelper.GameIsNotOver()
  }

  def FindACreature(id : Long): (Long,Creature) = {
    var d = World.curentAliveMembers.filter(_._1 == id)
    if(d.nonEmpty){
      return d(0)
    }
    return null
  }
  def FindACreature(name: String): (Long,Creature) = {
    var d = World.curentAliveMembers.filter(_._2.name == name)
    if(d.nonEmpty){
      return d(0)
    }
    return null
  }
  def MoveACreature(vertexId: VertexId, xPos : Int, yPos : Int): Unit = {
    var crea = curentAliveMembers.filter(_._1 == vertexId)
    if(crea.nonEmpty){
      crea(0)._2.xPos = xPos
      crea(0)._2.yPos = xPos
      if(!classOf[Orc].isInstance(crea(0)._2)) println(crea(0)._2.name + " se déplace en: " + crea(0)._2.xPos+ ","+crea(0)._2.yPos)
    }
  }
  def SetNeedHelp(id : Long): Unit ={
    if(curentAliveMembers.filter(_._1 == id).size != 0){
      curentAliveMembers.filter(_._1 == id)(0)._2.needHelp = true
    }
    }
  def Update(i : Int) = {
    turn = i
    UpdateGraph(i)
    UpdateMessages()
    UpdateCurrentMembers()
  }
}
