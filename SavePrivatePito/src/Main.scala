import Creatures.{Creature, Orc, Solar}
import Helper.ConditionHelper
import World.World
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.graphx._

import scala.collection.mutable.ArrayBuffer

object Main {


  var world = World

  def main(args: Array[String]): Unit = {

    val conf = new SparkConf()
      .setAppName("Super Jeu")
      .setMaster("local[*]") //For local tests
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")

    val sc = new SparkContext(conf)
    sc.setLogLevel("ERROR")
    sc.setCheckpointDir("/tmp")

    //Initialisation du monde pour le premier combat
    world.InitSecondFight(sc)
   // world.InitFirstFight(sc)

    var i =0
    while (!World.isGameOver){
      i+=1
      println("--------- turn "+i+" il reste "+World.curentAliveMembers.size+" créatures ------------")
      world.Update(i)
    }
    world.UpdateGraph(i)
    if(World.winner == 0) print("Bien joué Pito est sauf")
    else print("Solar à était terassé et Pito c'est retrouvé offert en sacrifice au orc")
  }
}
