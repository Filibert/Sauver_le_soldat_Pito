package Helper

import breeze.linalg.sum

object Dice {
  def ThrowDice(diceNumbers: Int, diceSize: Int): Int = {
    val r = scala.util.Random
    var sum = 0
    for (i <- 0 until diceNumbers){
      sum += r.nextInt(diceSize) + 1
    }
    sum
  }
}
