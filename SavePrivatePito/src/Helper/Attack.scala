package Helper

import scala.collection.mutable.ArrayBuffer

class Attack (val attackFixedDamage : Int, val diceNb : Int, val diceValue : Int, var precisionArray : ArrayBuffer[Int]) extends  Serializable {
  def calculateDamagesAndPrecision(step : Int): (Int, Int) = {
    var Precision = Dice.ThrowDice(1, 20)
    if(Precision == 20 ) Precision = 10000
    else Precision += precisionArray(step)
    val Damage = attackFixedDamage + Dice.ThrowDice(diceNb,diceValue)
    (Damage, Precision)
  }
}
