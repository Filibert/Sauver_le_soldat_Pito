package Helper

import Creatures.Creature

import scala.collection.mutable.ArrayBuffer

object ConditionHelper {
  def GameIsNotOver(): Boolean = {

    var lastCreature :Creature = null
    World.World.curentAliveMembers.foreach(f => {
      if(lastCreature != null){
        if(f._2.team != lastCreature.team)
          return true
      }
      else
        lastCreature = f._2
    })
    World.World.winner = lastCreature.team
    return false
  }
}
