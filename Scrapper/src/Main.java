import com.jaunt.Element;
import com.jaunt.Elements;
import com.jaunt.UserAgent;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        UserAgent userAgent = new UserAgent();
        Elements links = new Elements();//create new userAgent (headless browser)
        Elements spellLinks = new Elements();//create new userAgent (headless browser)

        List<String> listUrl = new LinkedList<String>();
        LinkedList<Monster> monsters = new LinkedList<>();
        String current = "deidkeifjeijfiejfijri";
        try{
            userAgent.visit("http://paizo.com/pathfinderRPG/prd/bestiary2/additionalMonsterIndex.html");          //visit google
            for(char alphabet = 'A'; alphabet <= 'Z'; alphabet++){
                links = userAgent.doc.findEvery("<ul id=\"index-monsters-"+alphabet+"\" title=\""+alphabet+" Monsters\">").findEvery("<a>");  //find search result links
                for(Element link : links){
                    String url =link.getAt("href");
                    if(url.contains("#"))
                        listUrl.add(url);           //print results
                }
            }

            for (String url : listUrl) {
                String heroName = url.substring(url.indexOf("#")).replace("#", "");
                if(current != null)
                    if(heroName.contains(current))
                        monsters.remove(monsters.getLast());
                current = heroName;
                Monster m = new Monster(heroName,new LinkedList<String>());
                userAgent.visit(url);          //visit google
                spellLinks = userAgent.doc.findEvery("<div>");
                Element goodDiv = null;
                Element h1 = null;
                Boolean youCanSave = false;
                List<Element> elements = new LinkedList<Element>() {
                };


                for (Element spellLink : spellLinks) {
                    //spellList.put(heroName,spellLink.getChildElements());
                    for (Element spellHeader : spellLink.getChildElements()) {
                        if(spellHeader.toString().contains("h1"))
                            if (spellHeader.toString().contains(heroName) )
                                youCanSave = true;
                            else if (spellHeader.toString().contains("h1") && youCanSave)
                                youCanSave = false;
                        if (youCanSave)
                            elements.add(spellHeader);

                    }
                }

                for (Element g : elements) {
                 //   spellList.get(heroName).add(g.toString());
                    for (Element j : g.getChildElements()) {
                        if (j.toString().contains("/pathfinderRPG/prd/coreRulebook/spells/")){
                            if(!m.spells.contains(j.toString().substring(j.toString().indexOf("s/"), j.toString().indexOf(".html")).replace("s/", "")))
                                m.spells.add(j.toString().substring(j.toString().indexOf("s/"), j.toString().indexOf(".html")).replace("s/", ""));
                        }
                        for (Element k : j.getChildElements()) {
                            if (k.toString().contains("/pathfinderRPG/prd/coreRulebook/spells/")){
                                if(!m.spells.contains(k.toString().substring(k.toString().indexOf("s/"), k.toString().indexOf(".html")).replace("s/", "")))
                                    m.spells.add(k.toString().substring(k.toString().indexOf("s/"), k.toString().indexOf(".html")).replace("s/", ""));
                            }
                            for (Element o : k.getChildElements()) {
                                if (o.toString().contains("/pathfinderRPG/prd/coreRulebook/spells/")){
                                    if(!m.spells.contains(o.toString().substring(o.toString().indexOf("s/"), o.toString().indexOf(".html")).replace("s/", "")))
                                        m.spells.add(o.toString().substring(o.toString().indexOf("s/"), o.toString().indexOf(".html")).replace("s/", ""));
                                }
                            }
                        }

                    }

                }
                monsters.add(m);
            }

            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(new File("result.json"), monsters);
        }catch(Exception e) {
            System.out.println(e.getMessage());
        }


    }
}
