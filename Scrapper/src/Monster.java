import java.util.LinkedList;

public class Monster {
    public String name;
    public LinkedList<String> spells;

    public Monster(String name, LinkedList<String> spell) {
        this.name = name;
        this.spells = spell;
    }
}
